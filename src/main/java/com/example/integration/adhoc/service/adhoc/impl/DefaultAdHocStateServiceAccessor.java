package com.example.integration.adhoc.service.adhoc.impl;

import com.comalatech.workflow.StateService;
import com.example.integration.adhoc.service.adhoc.AdHocStateServiceAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;

public class DefaultAdHocStateServiceAccessor implements AdHocStateServiceAccessor {

    private static final Logger logger = LoggerFactory.getLogger(DefaultAdHocStateServiceAccessor.class);

    private final ApplicationContext applicationContext;


    public DefaultAdHocStateServiceAccessor(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }


    @Override
    public boolean isAdHocWorkflowsInstalled() {
        return this.getService() != null;
    }


    @Override
    public StateService getStateService() {
        StateService stateService = this.getService();
        if (stateService == null) {
            throw new IllegalStateException("Ad Hoc Workflows is not installed.");
        }

        return stateService;
    }


    private StateService getService() {
        long before = System.currentTimeMillis();
        StateService result = null;

        Class<?> stateServiceFactoryClass = null;
        try {
            // If the {@link StateService} class is found on the classpath, Ad Hoc Workflows is
            // installed.
            getClass().getClassLoader().loadClass("com.comalatech.workflow.StateService");
            // If no exception has been thrown yet, Ad Hoc Workflows is available.
            stateServiceFactoryClass = getClass().getClassLoader().loadClass(
                    "com.example.integration.adhoc.service.adhoc.impl.StateServiceFactory");
        } catch (Exception e) {
            logger.info("The necessary Ad Hoc Workflows class is unavailable.");
        }

        if (stateServiceFactoryClass != null) {
            try {
                StateServiceFactory factory = (StateServiceFactory) applicationContext
                        .getAutowireCapableBeanFactory().createBean(stateServiceFactoryClass,
                                AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR, false);
                result = factory.get();
                factory.destroy();
            } catch (Exception e) {
                logger.debug("Could not create StateService", e);
            }
        }

        long after = System.currentTimeMillis();
        logger.debug("Retrieving the StateService took {} ms.", after - before);

        return result;
    }

}
