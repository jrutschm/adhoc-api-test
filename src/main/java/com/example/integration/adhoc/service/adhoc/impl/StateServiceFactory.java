package com.example.integration.adhoc.service.adhoc.impl;

import com.comalatech.workflow.StateService;

/**
 * A service factory to safely create {@link StateService} instances if and only if Ad Hoc Workflows
 * is installed.
 */
public class StateServiceFactory extends OptionalService<StateService> {

    public StateServiceFactory() {
        super(StateService.class);
    }


    public StateService get() {
        // If this class is being loaded, that means that a sufficient version of Ad Hoc Workflows
        // is found. It should be safe to call getService without worrying about the underlying
        // service tracker not finding the Ad Hoc Workflows instance.
        StateService stateService = getService();
        return stateService;
    }

}
