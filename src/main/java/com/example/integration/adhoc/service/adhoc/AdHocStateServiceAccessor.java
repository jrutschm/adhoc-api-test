package com.example.integration.adhoc.service.adhoc;

import com.comalatech.workflow.StateService;

public interface AdHocStateServiceAccessor {

    boolean isAdHocWorkflowsInstalled();

    StateService getStateService();

}
