package com.example.integration.adhoc.service.adhoc.impl;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.osgi.context.BundleContextAware;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * An optional service that may or may not exist. This class is completely generic
 * and can be overridden to be used for any optional service.
 *
 */
public abstract class OptionalService<T> implements BundleContextAware, DisposableBean
{
    private final Class<T> type;

    private ServiceTracker tracker;

    public OptionalService(Class<T> type)
    {
        this.type = checkNotNull(type, "type");
    }

    /**
     * Returns the service (of type {@code T}) if it exists, or {@code null} if not
     * @return the service (of type {@code T}) if it exists, or {@code null} if not
     */
    protected final T getService()
    {
        return type.cast(tracker.getService());
    }

    public final void destroy()
    {
        tracker.close();
    }

    public final void setBundleContext(BundleContext bundleContext)
    {
        tracker = new ServiceTracker(bundleContext, type.getName(), null);
        tracker.open();
    }
}
