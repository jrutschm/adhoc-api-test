package com.example.integration.adhoc.rest;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.comalatech.workflow.StateService;
import com.comalatech.workflow.model.State;
import com.example.integration.adhoc.service.adhoc.AdHocStateServiceAccessor;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import java.io.StringWriter;
import java.util.List;

@Path("/state")
public class AdHocApiOptionalTestResource {

    private static final Logger logger = LoggerFactory.getLogger(AdHocApiOptionalTestResource.class);

    private final PageManager pageManager;
    private final AdHocStateServiceAccessor adHocStateServiceAccessor;


    public AdHocApiOptionalTestResource(PageManager pageManager,
            AdHocStateServiceAccessor adHocStateServiceAccessor) {

        this.pageManager = pageManager;
        this.adHocStateServiceAccessor = adHocStateServiceAccessor;
    }


    @GET
    @Path("/{pageId}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getCurrent(@PathParam("pageId") final long pageId) {
        Page page = pageManager.getPage(pageId);

        if (!adHocStateServiceAccessor.isAdHocWorkflowsInstalled()) {
            return Response.status(503).entity("Ad Hoc Workflows is not installed.").build();
        }

        try {
            StateService stateService = adHocStateServiceAccessor.getStateService();

            // Get the list of all state changes, always attached to the latest version of a page.
            List<? extends State> allStates = stateService.getStates(page.getLatestVersion());

            State state = null;

            // Loop over all states to find the one corresponding to the version of the page.
            for (State s : allStates) {
                if (s.getContentVersion() > page.getVersion()) {
                    break;
                } else {
                    state = s;
                }
            }

            StringWriter sw = new StringWriter();
            sw.write("Page version:" + page.getVersion());
            sw.write("\n");
            sw.write("State Name:" + state.getName());
            sw.write("\n");
            if ("Approved".equals(state.getName())) {
                sw.write("Page is approved.");
                sw.write("\n");
                sw.write("Approval date:" + state.getDate());
            } else {
                sw.write("Page is draft");
            }

            return Response.ok(sw.toString()).build();
        } catch (Throwable t) {
            logger.error("Unexpected error.", t);
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(ExceptionUtils.getStackTrace(t)).build();
        }
    }

}
